import _ from 'underscore';

// Hierarchy example
let roleHierarchy = [
	{
		rolename: 'SUPERADMIN',
		rolelabel: 'Supreme',
		extend: ['ADMIN']
	},
	{
		rolename: 'ADMIN',
		rolelabel: 'Administrator',
		extend: ['MANAGER']
	},
	{
		rolename: 'MANAGER',
		rolelabel: 'Manager',
		extend: ['USER']
	},
	{
		rolename: 'USER',
		rolelabel: 'User'
	},
	{
		rolename: 'TEST',
		rolelabel: 'Test',
		extend: ['SOMEGUY', 'OTHERGUY']
	},
	{
		rolename: 'SOMEGUY',
		rolelabel: 'Someguy',
		actions: [
			{name: 'SOME_GLOBAL_ACTION', checker() {
				return Promise.resolve({check: true});
			}}
		]
	},
	{
		rolename: 'OTHERGUY',
		rolelabel: 'Other guy',
	},
	{
		rolename: 'ANONYM',
		rolelabel: 'Anonym',
	}
];

class Roles {
	static getRoleHierarchy() {
	// get role hierarchy from some source
		return roleHierarchy;
	}

	constructor() {
		this.hierarchy = Roles.getRoleHierarchy();
		this.permissions = this.buildGrapes(this.hierarchy);
	}

	buildGrapes(hierarchy) {
		let grapes = {};

		hierarchy.forEach((item) => {
			grapes[item.rolename] = {
				rolename: item.rolename,
				extend: item.extend || [],
				actions: item.actions || []
			}

			Object.defineProperty(grapes[item.rolename], 'dynamic_ext', {
				get: () => this.extendRole(grapes[item.rolename], grapes)
			});

			Object.defineProperty(grapes[item.rolename], 'dynamic_act', {
				get: () => this.extendActions(grapes[item.rolename], grapes)
			});
		})

		hierarchy.forEach((item) => {
			grapes[item.rolename]['ext'] = grapes[item.rolename]['dynamic_ext'];
		});

		return grapes;
	}

	extendRole(role, grapes) {
		if(role.extend === undefined) return [role.rolename];

		let result = [];

		for(let i = 0; i < role.extend.length; i++) {
			let rolename = role.extend[i];
			let subRoles = grapes[rolename] && grapes[rolename].dynamic_ext;

			if( subRoles && subRoles.length > 0 ) result = result.concat(subRoles);
		}

		result.push(role.rolename);

		return result;
	}

	extendActions(role, grapes) {
		let result = role.actions || [];
		let roles = role.dynamic_ext;

		for(let i = 0; i < roles.length; i++) {
			if(grapes[roles[i]].actions && grapes[roles[i]].actions.length > 0) {
				result = result.concat(grapes[roles[i]].actions);
			}
		}

		return result;
	}

	defineAction(rolenames, action) {
		if(typeof rolenames == 'String') rolenames = [rolenames];

		rolenames.forEach((rolename) => {
			if(this.permissions[rolename] == undefined) return;
			this.permissions[rolename].actions.push(action);
		});
	}

	get rolesList() {
		return this.hierarchy.map((role) => role.rolename)
	}
}

let roles = new Roles();

export function defaultChecker(role, action, checker) {
	let accessError = { check: false, error: new Error('Access denied') }

	if(role === undefined || action === undefined) {
		return Promise.resolve(accessError);
	} else {
		return Promise.resolve({check: true})
	}
}

export function checkPermission(action_name, rolename, options) {
	let role = roles.permissions[rolename];
	let action = (role !== undefined) ? _.findWhere(role.dynamic_act, {name: action_name}) : undefined;

	let checkResult =
		(role == undefined || action == undefined || action.checker === undefined) ?
		defaultChecker(role, action) :
		action.checker(rolename, options);

	if(checkResult instanceof Promise) {
		return checkResult;
	} else {
		return Promise.resolve(checkResult);
	}
}

export function defineAction(role_names, action_name, checker) {
	if(typeof role_names == 'String') role_names = [role_names];

	roles.defineAction(role_names, { name: action_name, checker })
}

export function getRolesList(rolename) {
	if(roles.permissions[rolename] == undefined) return [];

	let rolesList = roles.permissions[rolename].ext;

	if(rolesList.indexOf('SUPERADMIN') != -1)
		rolesList.splice(rolesList.indexOf('SUPERADMIN'), 1);

	return rolesList.map(item => {
		for(let i = 0; i < roles.hierarchy.length; i++) {
			if(roles.hierarchy[i].rolename == item)
				return {
					rolename: item,
					rolelabel: roles.hierarchy[i].rolelabel
				}
		}
	})
}

export default roles;
