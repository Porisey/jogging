import passport from 'passport';
import localPass from 'passport-local';
import jwtPass from 'passport-jwt';
import mongoose from 'mongoose';

import config from '../config';

export let roles = {
	'ADMIN': {},
	'MANAGER': {},
	'USER': {}
}

export function init(app) {
	passport.use('local', new localPass.Strategy({
		usernameField: 'auth[email]',
		passwordField: 'auth[password]'
	}, checkUserPassword));

	passport.use('jwt', new jwtPass.Strategy({
		jwtFromRequest: jwtPass.ExtractJwt.fromAuthHeaderAsBearerToken(),
		secretOrKey: config.secret,
	}, checkUserJWT));
}

export function checkUserPassword(email, password, done) {
	mongoose.model('users').findOne({email: email})
		.then(function(user) {
			if(!user || !user.checkPassword(password))
				return done(null, false, {errors: {'email or password': 'is invalid'}});

			return done(null, user);
		}).catch(done);
}

export function checkUserJWT(jwt_payload, done) {
	mongoose.model('users').findById(jwt_payload.id)
		.then(function(user) {
			return done(null, (!user) ? false : user);
		})
}

function localOptionalAuth(req, res, next) {

	passport.authenticate('local', { session: false },
	function(err, user, info) {
		if(err !== null) next(err);
		if(!!user) {
			req.authenticated = true;
			req.user = user
		} else {
			req.authenticated = false;
		}
		next();
	})(req, res, next);
}

function jwtOptionalAuth(req, res, next) {
	passport.authenticate('jwt', { session: false },
	function(err, user, info) {
		if(err !== null) next(err);
		if(!!user) {
			req.authenticated = true;
			req.user = user
		} else {
			req.authenticated = false;
		}
		next()
	})(req, res, next);
}

export default {
	local: {
		optional: localOptionalAuth,
		required: passport.authenticate('local', { session: false })
	},
	jwt: {
		optional: jwtOptionalAuth,
		required: passport.authenticate("jwt", { session: false })
	}
};
