export default {
	isProd: (process.env.NODE_ENV === 'production'),
	secret: 'fc7002cecc023577501f1eb63bf7b3a4',
	db: 'mongodb://localhost:27017/jogging',
	port: 4500
}

