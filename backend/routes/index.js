import api from './api';

let router = require('express').Router();

router.use('/api', api);

export default router;