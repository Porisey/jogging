import mongoose from 'mongoose';
import { checkPermission, defineAction, getRolesList } from '../../auth/roles';

const singleton = Symbol();
const singletonCheck = Symbol();

export let actions;

export class User {
	static get instance() {
		if(this[singleton] === undefined) this[singleton] = new User(singletonCheck);
		return this[singleton];
	}

	static get actions() {
		return (actions == undefined) ? User.definePermissions() : actions;
	}

	static definePermissions() {
		actions = {
			'DELETE_OTHER': Symbol(),
			'DELETE_SPECIFIC': Symbol(),
			'DELETE_SELF': Symbol(),
			'GET_USERS_LIST': Symbol(),
			'GET_RECORDS_LIST': Symbol(),
		}

		defineAction(['USER'], actions.DELETE_SELF, (role, options) => {
			if(role == 'SUPERADMIN') {
				return Promise.resolve({ check: false, error: new Error('Cant remove superadmin') });
			} else {
				return Promise.resolve({ check: true });
			}
		});

		defineAction(['MANAGER'], actions.GET_USERS_LIST );
		defineAction(['MANAGER'], actions.DELETE_OTHER);
		defineAction(['ADMIN'], actions.GET_RECORDS_LIST);
		defineAction(['MANAGER'], actions.DELETE_SPECIFIC, User.checkUserDeletePermission);

		return actions
	}

	static checkUserDeletePermission(rolename, options) {
		let deny = Promise.resolve({check: false, error: new Error('Access denied') });
		let notFound = Promise.resolve({ check: false, error: new Error('User not found') });

		if(!options || !options.targetId) return notFound;

		return mongoose.model('users').findById(options.targetId)
			.then(function(user) {
				let allow = Promise.resolve({check: true, user: user});

				if(user == null) return notFound;
				if(user.role == 'SUPERADMIN') return deny;

				switch(rolename) {
					case 'SUPERADMIN':
						return allow;
					case 'ADMIN':
						return (['MANAGER', 'USER'].indexOf(user.role) != -1) ? allow : deny;
					case 'MANAGER':
						return (['USER'].indexOf(user.role) != -1) ? allow : deny;
					default:
						return deny;
				}
			}).catch((err)=> {
				return Promise.resolve({check: false, error: notFound });
			})
	}

	constructor(check) {
		if(check != singletonCheck) {
			throw new Error("Don't try to create singleton manually");
		}

		User.definePermissions()
	}

	login(req, res, next) {
		req.user.read()
			.then(result => {
				return res.json({
					user: result,
					token: req.user.getToken()
				})
			})
			.catch(next);
	}

	logout(req, res, next) {
		if(req.user !== undefined) {
			req.logout()
		}

		res.json({
			status: 'logout'
		});
	}

	checkout(req, res, next) {
		req.user.read()
			.then(result => res.json({ user: result }))
			.catch(next)
	}

	update(req, res, next) {
		let model = mongoose.model('users');

		let userData = {
			email: req.body.user.email,
			firstname: req.body.user.firstname,
			surname: req.body.user.surname,
		}

		if(req.user) {
			let acceptableRoles = getRolesList(req.user.role).map(role => role.rolename);
			if(acceptableRoles.indexOf(req.body.user.role) != -1)
				userData.role = req.body.user.role;
		}

		if(req.body.user.password) {
			userData.password = req.body.user.password
		}

		checkPermission(User.actions['GET_USERS_LIST'], req.user.role)
			.then((result) => {
				if(!result.check) Promise.reject(result.error)
			})
			.then(() => model.findById(req.body.user._id))
			.then(result => result.update(userData))
			.then(result => result.read())
			.then(result => res.json({ user: result }))
			.catch(next);
	}

	register(req, res, next) {
		let model = mongoose.model('users');
		let user = new model();

		let userData = {
			email: req.body.user.email,
			firstname: req.body.user.firstname,
			surname: req.body.user.surname,
			password: req.body.user.password,
		}

		if(req.user) {
			let acceptableRoles = getRolesList(req.user.role).map(role => role.rolename);
			if(acceptableRoles.indexOf(req.body.user.role) != -1)
				userData.role = req.body.user.role;
		}

		user.create(userData)
			.then((user) => user.read())
			.then((userInfo) => {
				return res.json({user: userInfo})
			}).catch(next);
	}

	list(req, res, next) {
		checkPermission(User.actions['GET_USERS_LIST'], req.user.role)
			.then((result) => {
				return (result.check) ?
					mongoose.model('users').find({}) :
					Promise.reject(result.error)
			})
			.then((users) => Promise.all(users.map(user => user.read())))
			.then((result) => res.json({result}))
			.catch(next)
	}

	delete(req, res, next) {
		let id = req.body.user.id ? req.body.user.id : this.user.id;

		checkPermission(User.actions['DELETE_SPECIFIC'], req.user.role, {targetId: id})
			.then((result) => {
				return (result.check) ?
					result.user.delete() :
					Promise.reject(result.error);
			})
			.then(() => {
				res.json({ info: 'User was deleted' });
			})
			.catch(next)
	}

	detail(req, res, next) {
		return checkPermission(User.actions['GET_USERS_LIST'], req.user.role)
			.then((result) => {
				return (result.check) ?
					mongoose.model('users').findById(req.body.user.id) :
					Promise.reject(result.error)
			})
			.then(result => {
				return res.json({ user: result });
			}).catch(next)
	}
}

export default User.instance;
