import express from 'express';
import passport from 'passport';

import user from './users';
import record from './records';
import auth from '../../auth';


let router = express.Router();


// User API routing
router
	.post('/user/login', auth.local.required, user.login)
	.post('/user/register', auth.jwt.optional, user.register)
	.get('/user/logout', auth.jwt.optional, user.logout)

	.all('/user/*', auth.jwt.required)
		.get('/user/checkout', user.checkout)
		.get('/user/list', user.list)
		.post('/user/update', user.update)
		.post('/user/delete', user.delete)
		.post('/user/detail', user.detail)


// Records API routing
router
	.all('/record/*', auth.jwt.required)
		.get('/record/list', record.list)
		.get('/record/all', record.all)
		.post('/record/create', record.create)
		.post('/record/delete', record.delete)
		.post('/record/update', record.update)
		.post('/record/detail', record.detail)


// Not found method handler
router
	.all('*', (req, res, next) => {
		next(new Error("Can't find requested api method"))
	})

export default router;
