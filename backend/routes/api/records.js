import mongoose from 'mongoose';

const singleton = Symbol();
const singletonCheck = Symbol();

export class Record {
	static get instance() {
		if(this[singleton] === undefined) this[singleton] = new Record(singletonCheck);
		return this[singleton];
	}

	constructor(check) {
		if(check != singletonCheck) {
			throw new Error("Don't try to create singleton manually");
		}
	}

	create(req, res, next) {
		let model = mongoose.model('records');
		let record = new model();
		let reqData = req.body

		if(reqData.record == undefined) {
			next(new Error('Bad request:' + JSON.stringify(reqData)))
		}

		let recordData = {
			distance: reqData.record.distance,
			date: reqData.record.date,
			seconds: reqData.record.seconds,
			userId: req.user._id
		}

		record.create(recordData)
			.then((result) => {
				res.json(result)
			}).catch(next)
	}

	delete(req, res, next) {
		let model = mongoose.model('records')

		model.delete(req.body.record.id)
			.then((item) => {
				res.json(item);
			}).catch(next);
	}

	update(req, res, next) {
		let model = mongoose.model('records')
		model.update(req.user, req.body)
			.then((item) => {
				res.json(item);
			}).catch(next);
	}

	list(req, res, next) {
		let model = mongoose.model('records')

		model.getList(req.user)
			.then((items) => {
				res.json({items});
			}).catch(next)
	}

	all(req, res, next) {
		let model = mongoose.model('records')

		model.getAll(req.user)
			.then((items) => {
				res.json({items});
			}).catch(next)
	}

	detail(req, res, next) {
		let model = mongoose.model('records')

		model.getDetail(req.body.record.id, req.user.role)
			.then((item) => {
				res.json({item});
			}).catch(next)
	}
}

export default Record.instance;
