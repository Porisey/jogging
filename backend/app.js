import express from 'express';
import bodyParser from 'body-parser';
import methodOverrider from 'method-override';
import session from 'express-session';
import errorhandler from 'errorhandler';
import mongoose from 'mongoose';
import passport from 'passport';
// import path from 'path';

import config from './config';
import { factory as modelsBuilder } from './models';
import {init as authInit} from './auth';
import routes from './routes';

let app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverrider());
app.use(express.static(__dirname + '/../public'));

app.use(session({
	secret: config.secret,
	cookie: { maxAge: 60000 },
	resave: false,
	saveUninitialized: false
}));

if (!config.isProd) {
	app.use(errorhandler());
	mongoose.set('debug', true);
}

mongoose.Promise = global.Promise;

mongoose.connect(config.db, { useMongoClient: true })
	.then((db) => {
		modelsBuilder(db);
		authInit(app);

		app.use(passport.initialize());
		app.use(passport.session());

		app.use(routes);

		// SPA
		app.use(function(req, res, next) { res.sendFile('index.html', { root: __dirname  + '/../public' }); });



		app.use(function(err, req, res, next) {
		  console.log(err.stack);

		  res.status(err.status || 500);

		  res.json({'errors': {
		    message: err.message,
		    error: err
		  }});
		});
	}).catch((err) => {
		throw err;
	});

export let server =
	app.listen( config.port, function(){
		console.log('Listening on port ' + server.address().port);
	});

export default app;
