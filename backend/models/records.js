import mongoose from 'mongoose';
import { checkPermission, defineAction } from '../auth/roles';

let actions;

class Record {
	static get actions() {
		return (actions == undefined) ? Record.definePermissions() : actions;
	}

	static definePermissions() {
		actions = {
			'GET_LIST': Symbol(),
			'GET_DETAIL': Symbol(),
			'GET_ALL': Symbol(),
		}

		defineAction(['ADMIN'], actions.GET_ALL);
		defineAction(['USER'], actions.GET_LIST);
		defineAction(['USER'], actions.GET_DETAIL);

		return actions;
	}

	static delete(id) {
		return mongoose.model('records').findById(id)
			.then((record) => {
				return (record === null) ? Promise.reject(new Error('Record not found')) : record.remove();
			})
			.then((record) => record._getData());
	}

	static getList(user) {
		return checkPermission(Record.actions['GET_LIST'], user.role)
			.then((result) => {
				if(result.check) {
					return mongoose.model('records').find({user: user._id}).populate('user')
						.select('id distance date seconds user')
				} else {
					return Promise.reject(result.error);
				}
		})
	}

	static getAll(user) {
		return checkPermission(Record.actions['GET_ALL'], user.role)
			.then((result) => {
				if(result.check) {
					return mongoose.model('records').find({}).populate('user')
						.select('id distance date seconds user')
				} else {
					return Promise.reject(result.error);
				}
		})
	}

	static getDetail(id, role) {
		return checkPermission(Record.actions['GET_DETAIL'], role)
			.then((result) => {
				if(result.check) {
					return mongoose.model('records').findById(id).populate('user')
						.select('id distance date seconds user')
				} else {
					return Promise.reject(result.error);
				}
		})
	}

	create(data) {
		this.distance = data.distance;
		this.date = new Date(data.date);
		this.seconds = data.seconds;
		this.user = data.userId;

		return this.save().then(() => this._getData());
	}

	static update(user, data) {
		return checkPermission(Record.actions['GET_LIST'], user.role)
			.then((result) => {
				if(result.check) {
					return mongoose.model('records')
						.findById(data.record._id)
						.then((record) => {
							if(record == null) return Promise.reject(new Error('404'))

							record.distance = data.record.distance;
							record.date = new Date(data.record.date);
							record.seconds = data.record.seconds;

							return record.save()
						})
						.then((record) => record.populate('user')._getData());
				} else {
					return Promise.reject(result.error);
				}
			})
	}

	_getData() {
		return {
			record: {
				_id: this._id.toString(),
				distance: this.distance,
				date: this.date,
				seconds: this.seconds,
				user: this.user
			}
		}
	}
}

let schema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'users'
	},
	distance: {
		type: String,
		required: [true, "Can't be blank"],
		index: true
	},
	date: {
		type: Date,
		required: [true, "can't be blank"],
	},
	seconds: {
		type: Number,
		required: [true, "can't be blank"],
	}
})

export default function (db) {
	schema.loadClass(Record);
	return mongoose.model('records', schema);
}
