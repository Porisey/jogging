import mongoose from 'mongoose';
import crypto from 'crypto';
import config from '../config';
import jwt from 'jsonwebtoken';
import { checkPermission, defineAction, getRolesList } from '../auth/roles';
import { actions as userApiActions } from '../routes/api/users';

class User {
	create(data) {
		this.email = data.email;
		this.firstname = data.firstname;
		this.surname = data.surname;
		this.setPassword(data.password);

		if(typeof data.role == 'string') this.role = data.role;

		return this.save();
	}

	read() {
		return this.getPermissionsList()
			.then((result) => {
				return {
					_id: this._id.toString(),
					firstname: this.firstname,
					surname: this.surname,
					email: this.email,
					role: this.role,
					permissions: result,
					available_roles: getRolesList(this.role)
				}
			})
	}

	update(data) {
		for(let prop in data) {
			if(prop == 'password') {
				this.setPassword(data[prop])
				continue;
			}

			if(data[prop] !== undefined)
				this[prop] = data[prop]
		}

		return this.save(data)
	}

	delete() {
		let id = this._id;

		return this.remove()
			.then(() => {
				let model = mongoose.model('records');
				return model.find({user: id}).remove()
			})
	}

	setPassword(password) {
		this.hash = this.getPasswordHash(password);
	}

	checkPassword(password) {
		return this.hash === this.getPasswordHash(password);
	}

	getPasswordHash(password) {
		return crypto.pbkdf2Sync(password, config.secret, 10000, 512, 'sha512').toString('hex');
	}

	getToken() {
		var today = new Date();
		var exp = new Date(today);
		exp.setDate(today.getDate() + 60);

		return jwt.sign({
			id: this._id,
			exp: parseInt(exp.getTime() / 1000),
		}, config.secret);
	}

	getPermissionsList() {
		let permissionToCheck = [
			'DELETE_SELF',
			'DELETE_OTHER',
			'GET_USERS_LIST',
			'GET_RECORDS_LIST'
		];

		let permissions = userApiActions;

		return Promise.all(permissionToCheck.map(
			(permission) => checkPermission(permissions[permission], this.role)
		)).then((results) => {
			return results.map((item, i) => {
				item.action_name = permissionToCheck[i];
				if(item.error !== undefined) item.error = item.error.message;
				return item
			})
		})
	}
}

let schema = new mongoose.Schema({
	firstname: {
		type: String,
		required: [true, "Can't be blank"],
		index: true
	},
	surname: {
		type: String,
		required: [true, "Can't be blank"],
		index: true
	},
	email: {
		type: String,
		lowercase: true,
		unique: true,
		required: [true, "can't be blank"],
		match: [/\S+@\S+\.\S+/, 'is invalid'],
		index: true
	},
	role: {
		type: String,
		default: 'USER'
	},
	hash: String
})

export default function (db) {
	schema.loadClass(User);

	schema.post('save', function(error, doc, next) {
		if (error.name === 'MongoError' && error.code === 11000) {
			next(new Error('User with same email already exists'));
		} else {
			next(error);
		}
	});

	return mongoose.model('users', schema);
}
