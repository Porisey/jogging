import users from './users';
import records from './records';

export function factory(db) {
	let models = {
		users: users(db),
		records: records(db)
	};

	models.users.findOne({'email': 'admin@mail.ru'})
		.then((user) => {
			if(user == null) {
				new models.users()
					.create({
						email: 'admin@mail.ru',
						password: 'admin',
						firstname: 'Admin',
						surname: 'Adminov',
						role: 'SUPERADMIN'
					})
			}
		})


	return models
}
