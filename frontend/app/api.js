import axios from 'axios';

let singleton = Symbol();

class Api {
	static get instance() {
		if(this[singleton] === undefined) this[singleton] = new Api();
		return this[singleton];
	}

	get token () {
		let token;
		if(localStorage !== undefined) token = localStorage.getItem('token')
		return this._token || token
	}

	set token(val) {
		if(localStorage !== undefined) localStorage.setItem('token', val)
		this._token = val
	}

	constructor(props) {
		this.defineConnectionParams()
		this.user = this.getUserApi()
		this.record = this.getRecordApi()
	}

	defineConnectionParams() {
		this.apiServer = axios.create()
		this.apiServer.defaults.headers.common['Authorization'] = 'Bearer ' + this.token;
	}

	getUserApi() {
		return {
			login: (params) => this.apiServer.post('/api/user/login/', params)
				.then((response) => {
					this.token = response.data.token
					this.defineConnectionParams()
					return response;
				}),

			logout: (params) => this.apiServer.get('/api/user/logout/')
				.then((response) => {
					this.token = '';
					this.defineConnectionParams()
					return response;
				}),

			checkout: (params) => this.apiServer.get('/api/user/checkout/')
				.then((response) => {
					this._token = response.data.token
					return response;
				}),

			register: (params) => this.apiServer.post('/api/user/register/', params),
			update: (params) => this.apiServer.post('/api/user/update', params),
			detail: (params) => this.apiServer.post('/api/user/detail', params),
			list: (params) => this.apiServer.get('/api/user/list/'),
			delete: (params) => this.apiServer.post('/api/user/delete/', params)
		}
	}

	getRecordApi() {
		return {
			list: (params) => this.apiServer.get('/api/record/list/'),
			all: (params) => this.apiServer.get('/api/record/all/'),
			create: (params) => this.apiServer.post('/api/record/create/', params),
			update: (params) => this.apiServer.post('/api/record/update/', params),
			delete: (params) => this.apiServer.post('/api/record/delete/', params),
			detail: (params) => this.apiServer.post('/api/record/detail/', params)
		}
	}
}


export default Api.instance;
