import { List } from 'immutable';
import api from '../api'

export const actions = {
	"DEFAULT": Symbol(),
	"RESET": Symbol()
}

export const initState = {
	current: {
		isAuthorized: !(api.token === '' || api.token == undefined),
		userInfo: {
			id: '',
			firstname: '',
			surname: '',
			email: '',
			permissions: [],
			role: '',
			available_roles: []
		}
	},
	list: {
		items: List([]),
		loading: true
	}
};

export default (state = initState, action) => {
	switch(action.type) {
		case actions.RESET:
			initState.current.isAuthorized = api.token != ''
			return initState;

		case actions.DEFAULT:
			return { ...state, ...action.data }

		default:
			return state
	}
}
