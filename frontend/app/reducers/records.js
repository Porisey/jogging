import { List } from 'immutable';

export const actions = {
	"DEFAULT": Symbol(),
	"RESET": Symbol(),
}

export const initState = {
	myRecords: List([]),
	allRecords: List([]),
	myRecordsLoading: true,
	myRecordsLoading: true
};

export default (state = initState, action) => {
	switch(action.type) {
		case actions.RESET:
			return initState;

		case actions.DEFAULT:
			return { ...state, ...action.data }
		default:
			return state
	}
}
