import { combineForms } from 'react-redux-form';

const registrationForm = {
	firstname: '',
	surname: '',
	email: '',
	password: '',
	repeat_password: ''
};

const authForm = {
	email: '',
	password: ''
};

const recordForm = {
	date: '',
	distance: '',
	time: ''
}

const recordEditForm = {
	id: '',
	email: '',
	date: '',
	distance: '',
	time: ''
}

const userEditForm = {
	id: '',
	email: ''
}



export default combineForms({
	registration: registrationForm,
	auth: authForm,
	user_edit: userEditForm,
	record: recordForm,
	record_edit: recordEditForm,
}, 'forms');
