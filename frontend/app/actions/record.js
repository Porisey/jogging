import { actions as formActions } from 'react-redux-form';
import { actions as recordActions } from '../reducers/records';
import { List } from 'immutable';

import api from '../api';
import store from '../store';
import Tools from '../tools';

import _ from 'underscore';

export default {
	create: (data) => {
		return (dispatch, getState) => {
			let params = {
				record: {
					...data.record,
					seconds: Tools.parseTime(data.record.time)
				}
			}

			api.record.create(params)
				.then((response) => {
					let state = getState();
					let records = state.records;
					let newRecord = response.data.record;

					newRecord.time = Tools.parseSeconds(response.data.record.seconds)

					dispatch({ type: recordActions['DEFAULT'], data: {
						myRecords: records.myRecords.push(newRecord),
						allRecords: records.myRecords.push(newRecord)
					}})

					dispatch(formActions.reset('forms.record'))
					dispatch(formActions.setSubmitted('forms.record', true));
				})
		}
	},

	update: (data) => {
		return (dispatch, getState) => {
			let params = {
				record: {
					...data.record,
					seconds: Tools.parseTime(data.record.time)
				}
			}

			api.record.update(params)
				.then((response) => {
					let state = getState();
					let records = state.records;
					let newRecord = response.data.record;
					let result = {};

					newRecord.time = Tools.parseTime(response.data.record.time)

					let myRecordsKey = records.myRecords.findKey((item) => item._id == newRecord._id)
					let allRecordsKey = records.allRecords.findKey((item) => item._id == newRecord._id)

					if(myRecordsKey === undefined) {
						result.myRecords = records.myRecords.push(newRecord)
					} else {
						result.myRecords = records.myRecords.update(myRecordsKey, () => newRecord)
					}

					if(allRecordsKey === undefined) {
						result.allRecords = records.allRecords.push(newRecord)
					} else {
						result.allRecords = records.allRecords.update(allRecordsKey, () => newRecord)
					}

					dispatch({ type: recordActions['DEFAULT'], data: {
						myRecords: result.myRecords,
						allRecords: result.allRecords
					}})

					dispatch(formActions.setSubmitted('forms.record_edit', true));
				})
		}
	},

	list: () => {
		return (dispatch, getState) => {
			let state = getState();
			let isNeedIndicateLoading = !(state.records.myRecords.size > 0)

			if(isNeedIndicateLoading)
				dispatch({type: recordActions['DEFAULT'], data: {myRecordsLoading: true}});

			api.record.list()
				.then((response) => {
					let list = response.data.items.map((item) => {
						item.time = Tools.parseSeconds(item.seconds)
						return item
					})

					dispatch({type: recordActions['DEFAULT'], data: {
						myRecords: List(list),
					}})

					if(isNeedIndicateLoading)
						dispatch({type: recordActions['DEFAULT'], data: { myRecordsLoading: false}});
				}).catch((err) => {
					console.log(err);
				})
		}
	},

	all: () => {
		return (dispatch, getState) => {
			let state = getState();
			let isNeedIndicateLoading = !(state.records.allRecords.size > 0)

			if(isNeedIndicateLoading)
				dispatch({type: recordActions['DEFAULT'], data: {allRecordsLoading: true} });

			api.record.all()
				.then((response) => {
					let list = response.data.items.map((item) => {
						item.time = Tools.parseSeconds(item.seconds)
						return item
					})

					let state = getState();

					dispatch({type: recordActions['DEFAULT'], data: {
						allRecords: List(list),
					}});

					if(isNeedIndicateLoading)
						dispatch({type: recordActions['DEFAULT'], data: {allRecordsLoading: false}});

				}).catch((err) => {
					console.log(err);
				})
		}
	},

	delete: (params) => {
		return (dispatch, getState) => {
			api.record.delete(params)
				.then((response) => {
					let state = getState();
					let id = response.data.record._id;
					let myRecords = state.records.myRecords.filter( record => record._id !== id );
					let allRecords = state.records.allRecords.filter( record => record._id !== id );

					dispatch({type: recordActions['DEFAULT'], data: {
						myRecords,
						allRecords
					}})
				}).catch((err) => {
					console.log(err)
				})
		}
	},

	detail: (params) => {
		return (dispatch, getState) => {
			let state = getState();
			let meRecords = state.records.myRecords;
			let allRecords = state.records.allRecords;

			let localRecords = meRecords.concat(allRecords);

			let target = localRecords.find((value) => {
				return value._id == params.id
			})

			let promise;

			if(target == undefined) {
				promise = api.record.detail({record: {id: params.id}})
					.then((response) => response.data.item)
			} else {
				promise = Promise.resolve(target)
			}

			promise.then((item) => {
				let result = {
					email: item.user.email,
					time: Tools.parseTime(item.seconds),
					...item
				}

				dispatch(formActions.change('forms.record_edit', result))
			})
		}
	}
}
