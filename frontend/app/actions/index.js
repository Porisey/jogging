import userActions from './user'
import recordActions from './record'

export default {
	user: userActions,
	record: recordActions
}
