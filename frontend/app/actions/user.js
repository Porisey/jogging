import { actions as formActions } from 'react-redux-form';
import { actions as userActions, initState as userInit } from '../reducers/users';
import { actions as recordActions, initState as recordInit } from '../reducers/records';
import { List, Map } from 'immutable';

import api from '../api';

import _ from 'underscore';

export default {
	register: (data) => {
		return (dispatch, getState) => {
			dispatch(formActions.setPending('forms.registration', true));

			api.user.register(data)
				.then((result) => {
					let state = getState();
					let users = state.users.list.items;
					let newUser = result.data.user;

					dispatch({ type: userActions['DEFAULT'], data: {
						list: { items: users.push(newUser) }
					}})

					dispatch(formActions.setPending('forms.registration', false));
					dispatch(formActions.reset('forms.registration'))
					dispatch(formActions.setSubmitted('forms.registration', true));
				})
				.catch((err) => {
					dispatch(formActions.setPending('forms.registration', false));
					dispatch(formActions.setErrors('forms.registration', {serverError: true}));
				})
		}
	},

	login: (data) => {
		return (dispatch, getState) => {
			api.user.login(data)
				.then((result) => {
					let userInfo = result.data.user;
					userInfo.permissions = renderPermissions(userInfo.permissions);

					dispatch({ type: userActions['DEFAULT'], data: {
						current: {
							isAuthorized: true,
							userInfo: userInfo
						}
					}});
				}).catch((status) => {
					dispatch(formActions.setErrors('forms.auth', {authError: true}));
				})
		}
	},

	logout: () => {
		return (dispatch, getState) => {
			api.user.logout()
				.then(() => {
					dispatch({ type: userActions['RESET']});
					dispatch({ type: recordActions['RESET']});
					dispatch(formActions.reset('forms.auth'));
					dispatch(formActions.reset('forms.record'));
					dispatch(formActions.reset('forms.registration'));
				})
		}
	},

	checkout: (data) => {
		return (dispatch, getState) => {
			api.user.checkout()
				.then((result) => {
					let userInfo = result.data.user;
					userInfo.permissions = renderPermissions(userInfo.permissions);

					dispatch({ type: userActions['DEFAULT'], data: {
						current: {
							isAuthorized: true,
							userInfo: userInfo
						}
					}});
				});
		}
	},

	list: () => {
		return (dispatch, getState) => {
			api.user.list()
				.then((response) => {
					let list = response.data.result

					dispatch({type: userActions['DEFAULT'], data: {
						list: { items: List(list), loading: false }
					}})
				}).catch((err) => {
					console.log(err);
				})
		}
	},

	delete: (id) => {
		return (dispatch, getState) => {
			api.user.delete({ user: {id} })
				.then((response) => {
					let state = getState();
					let users = state.users.list.items
					let newUsers = users.filter( user => user._id !== id );

					dispatch({
						type: userActions['DEFAULT'],
						data: {
							list: { items: newUsers , loading: false}
						}
					})
				}).catch((err) => {})
		}
	},

	detail: (params) => {
		return (dispatch, getState) => {
			let state = getState();
			let localUsers = state.users.list.items;

			let target = localUsers.find((value) =>  value._id == params.id)

			let promise;

			if(target == undefined) {
				promise = api.user.detail({user: {id: params.id}})
					.then((response) => {
						return response.data.user
					})
			} else {
				promise = Promise.resolve(target)
			}

			promise.then((user) => {
				let result = { email: user.email, ...user }
				dispatch(formActions.change('forms.user_edit', result))
			})
		}
	},

	edit: (params) => {
		return (dispatch, getState) => {
			dispatch(formActions.setPending('forms.user_edit', true));

			api.user.update({ user: params.user })
				.then(response => {
					let state = getState();
					let users = state.users.list.items;
					let changedUser = response.data.user;

					users = users.set(users.findIndex(function(item) {
					  return item._id === changedUser._id;
					}), changedUser);

					dispatch({ type: userActions['DEFAULT'], data: {
						list: { items: users }
					}})

					dispatch(formActions.setPending('forms.user_edit', false));
					dispatch(formActions.setSubmitted('forms.user_edit', true));
				})
				.catch((err) => {
					dispatch(formActions.setPending('forms.user_edit', false));
					dispatch(formActions.setErrors('forms.user_edit', {serverError: true}));
				})
		}
	}
}

function renderPermissions(permissions) {
	let result = {}

	permissions.map((item) => {
		result[item.action_name] = item.check;
	})

	return result;
}
