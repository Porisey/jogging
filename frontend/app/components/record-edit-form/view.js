import React from 'react';
import { Errors, Form, Control } from 'react-redux-form';
import DatePicker from '../datepicker';

export default function (props) {
	return (
		<div className="b-default-form">
			<div className="b-default-form__header">
				<div className="b-default-form__title">{(props.formType == 'add') ? 'Add' : 'Edit'} record</div>
			</div>

			<Form model="forms.record_edit" onSubmit={ function (data) { props.handleSubmit(data); } }
				validators={ props.validators } onChange={ function (data) { props.handleChanges(data) } } >

				<div className="b-default-form__section b-default-form__section_cols">


					{ (props.formType == 'edit') &&
						<div className="b-default-form__full-field">
							<div className="b-default-form__input-holder">
								<Control.text model=".email" className="u-input u-input_full" placeholder="Email"/>
							</div>
							<Errors className="b-default-form__errors" model=".email" { ...props.errorParams } />
						</div>
					}


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".date" component={ DatePicker }
								className="u-input u-input_full" placeholder="Date"/>
						</div>
						<Errors className="b-default-form__errors" model=".date" { ...props.errorParams } />
					</div>


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".distance" className="u-input u-input_full" placeholder="Distance"
								getRef={ (node) => props.initMask('number', node) }/>
						</div>
						<Errors className="b-default-form__errors" model=".distance" { ...props.errorParams } />
					</div>


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".time" className="u-input u-input_full" placeholder="Time"
								getRef={ (node) => props.initMask('time', node) }/>
						</div>
						<Errors className="b-default-form__errors" model=".time" { ...props.errorParams } />
					</div>


				</div>
				{ props.formState.submitted && <div className="b-default-form__section">Record saved!</div> }
				<div className="b-default-form__section">
					<Errors className="b-default-form__errors" model="forms.registration" { ...props.errorParams } />
					<div className="b-default-form__submit-holder">
						<input className="u-button" type="submit" value="SUBMIT" />
					</div>
				</div>
			</Form>
		</div>
	)
}
