import React from 'react';
import { connect } from 'react-redux';
import { actions as formActions } from 'react-redux-form';

import store from '../../store';
import actions from '../../actions';
import Tools from '../../tools';
import View from './view';

class RegistrationForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
		this.defineFormValidators()
		this.defineErrorParams()

		if(props.targetId != undefined) {
			store.dispatch(actions.user.getEditDetail({id: props.targetId}));
		} else {
			store.dispatch(formActions.reset('forms.registration'));
		}
	}

	render() {
		return (
			<View
				user={ this.props.user }
				roles_list={ this.mapRolesToSelect(this.props.user.available_roles) }
				formType={ this.props.formType }
				formState={ this.props.formState }
				handleSubmit={ this.handleSubmit.bind(this) }
				handleChanges={ this.handleChanges.bind(this) }
				validators={ this.validators }
				errorParams={ this.errorParams }/>
		)
	}

	handleSubmit(formData) {
		let data = {
			...formData,
			role: formData.role ? formData.role.value : ''
		}
		store.dispatch(actions.user.register({'user': data}));
	}

	handleChanges(formData) {
		this.resetSubmit(formData);
		this.resetServerErrors(formData);
	}

	componentWillUnmount() {
		this.handleChanges()
	}

	resetSubmit(formData) {
		if(this.props.formState.submitted)
			store.dispatch(formActions.setSubmitted('forms.registration', false));
	}

	resetServerErrors(formData) {
		if(this.props.formState.errors.serverError)
			store.dispatch(formActions.setErrors('forms.registration', { serverError: false }));
	}

	defineFormValidators() {
		let required = Tools.formValidators.required;
		let isEmail = Tools.formValidators.isEmail;

		this.validators = {
			'': {
				passconfirm: (vals) => vals.password === vals.repeat_password,
			},

			firstname: { required },
			surname: { required },
			email: { required, isEmail },
			password: { required }
		}
	}

	defineErrorParams() {
		this.errorParams = {
			messages: {
				required: 'Required field',
				isEmail: 'Incorrect email',
				serverError: 'Server unavailable'
			},
			show: {
				touched: true,
				focus: false
			}
		}
	}

	mapRolesToSelect(roles) {
		return roles.map((role) => ({value: role.rolename, label: role.rolelabel}))
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current.userInfo,
		formState: store.forms.forms.registration.$form
	};
};

export default connect(mapStateToProps)(RegistrationForm);
