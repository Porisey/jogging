import React from 'react';
import Components from '../';
import { Redirect, Route, Switch, Link } from 'react-router-dom';

export default function (props) {
	return (
		<main className="b-layout__main">
			<div className="u-container">
				{ !props.user.isAuthorized && <Redirect to="/users/add"/>}

				<Route path='/reports' render={() => {
					return (
						<div className="b-layout__item b-layout__item_small">
							<Components.ReportsTable/>
						</div>
					)
				}}/>
			</div>
		</main>
	);
}
