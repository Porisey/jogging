import App from './app'
import Header from './header'
import UserTitle from './user-title'
import DefaultMenu from './default-menu'
import RegistrationForm from './registration-form'
import UserEditForm from './user-edit-form'
import Users from './users'
import Records from './records'
import ActionTable from './action-table'
import UsersTable from './users-table'
import RecordForm from './record-form'
import RecordEditForm from './record-edit-form'
import RecordsTable from './records-table'
import Datepicker from './datepicker'
import Inputmask from './inputmask'
import ReportsTable from './reports-table'
import Reports from './reports'

export default {
	App,
	Header,
	UserTitle,
	DefaultMenu,
	RegistrationForm,
	UserEditForm,
	Users,
	Records,
	ActionTable,
	UsersTable,
	RecordForm,
	RecordEditForm,
	RecordsTable,
	Datepicker,
	Inputmask,
	ReportsTable,
	Reports
}
