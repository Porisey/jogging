import React from 'react';
import Components from '../';

export default function (props) {
	return (
		<div className="b-header">
			<div className="b-header__top-section">
				<div className="u-container">
					<div className="b-header__logo-holder">
						<a className="b-header__logo">Jogging App</a>
					</div>
					<div className="b-header__user-ui">
						<Components.UserTitle/>
					</div>
				</div>
			</div>
			{props.user.isAuthorized &&
				<div className="b-header__menu-section">
					<div className="u-container">
						<div className="b-header__menu">
							<Components.DefaultMenu/>
						</div>
					</div>
				</div>
			}
		</div>
	);
}
