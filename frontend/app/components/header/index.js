import React from 'react';
import { connect } from 'react-redux';
import View from './view';

class Header extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (
			<View user={ this.props.user } />
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current
	};
}

export default connect(mapStateToProps)(Header);
