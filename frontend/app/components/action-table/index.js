import React from 'react';
import { connect } from 'react-redux';
import View from './view';

class ActionTable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (
			<View
				{ ...this.props }
				table={ this.buildTable(this.props.tableData) } />
		)
	}

	buildTable(data = {labels: {}, items: [], actions:[] }) {
		let result = {headers: {}, body: [], head: [] };

		for(let prop in data.labels) {
			if(data.labels[prop] == false) continue;
			if(result.headers[prop] == undefined ) {
				result.headers[prop] = data.labels[prop] || prop;
			}
		}


		this.defineSort(data.items)
			.forEach((item) => {
				let row = []

				Object.defineProperty(row, '_src', { value: item });

				for(let prop in result.headers) {
					row.push({render: item[prop] || ''});
				}

				data.actions.forEach((item) => {
					row.push({
						render: item.label,
						action: item.action
					})
				});

				result.body.push(row);
			});


		for(let prop in result.headers) {
			let item = {render: result.headers[prop]};

			if(this.props.sortField == prop) {
				item.sortBtn = <i className="fa fa-sort b-default-table__sort-btn" aria-hidden="true" onClick={ this.props.changeSort }></i>
			}
			result.head.push(item);
		}

		data.actions.forEach((item) => {
			result.head.push({render: item.header});
		})

		result.head = [result.head];

		return result;
	}

	defineSort(items) {
		if(typeof this.props.sorter == 'function') {
			return items.sort(this.props.sorter);
		} else if(this.props.sortField != undefined) {
			return items.sort((a, b) => {
				let sort = a[this.props.sortField] - b[this.props.sortField];
				return sort;
			})
		}

		return items;
	}
}

const mapStateToProps = (store) => {
	return {};
}

export default connect(mapStateToProps)(ActionTable);

