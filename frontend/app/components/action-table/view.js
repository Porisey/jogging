import React from 'react';
import Components from '../';
import { Link } from 'react-router-dom';

export default function (props) {
	let build = (type, data) => {
		return (data.map((row, index) => (
			<div className="b-default-table__tr" key={ !row.id ? index : row.id }>
				{ row.map((item, index) => (
					<div className={ type == 'head' ? "b-default-table__th" : "b-default-table__td" }
						onClick={() => { item.action ? item.action(row._src) : null } }
						key={!item.id ? index : item.id}>{ item.render }{ item.sortBtn }</div>
				)) }
			</div>)
		))
	};

	return (
		<div className="b-default-table">
			{ props.loading === false && props.table.body.length > 0 &&
				<div className="b-default-table__table">
					{ build('head', props.table.head) }
					{ build('body', props.table.body) }
				</div>
			}

			{ props.loading &&
				<div>Loading...</div>
			}

			{ props.loading === false && props.table.body.length == 0 &&
				<div>You doesn't post any entries yet</div>
			}

			{ props.addLink &&
				<div className="b-default-table__add-btn">
					<Link to={ props.addLink } className="u-add-link">
						<i className="fa fa-plus-square-o"></i>
						<span> Add new record</span>
					</Link>
				</div>
			}
		</div>
	);
}
