import React from 'react';
import { connect } from 'react-redux';
import View from './view';
import actions from '../../actions';
import store from '../../store';
import api from '../../api';
import { withRouter } from 'react-router-dom';

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}

		if(api.token != '')
			store.dispatch(actions.user.checkout())
	}

	render() {
		return (
			<View user={ this.props.user } match={ this.props.match }/>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current
	};
}

export default connect(mapStateToProps)(withRouter(App));
