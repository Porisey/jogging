import React from 'react';
import Components from '../';
import { Redirect, Route, Switch, Link } from 'react-router-dom';
import { matchPath } from 'react-router'

export default function (props) {
	return (
		<div className="b-layout">
			<header className="b-layout__header">
				<Components.Header/>
			</header>

			<Switch>
				<Route path="/users" component={Components.Users} />
				{ !props.user.isAuthorized && <Redirect to="/users/"/> }

				<Route path="/records" component={Components.Records} />
				<Route path="/reports" component={Components.Reports} />
				<Redirect from="/" to="/records"/>
			</Switch>
		</div>
	);
}
