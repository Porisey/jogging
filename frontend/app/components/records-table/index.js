import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import moment from 'moment';
import accounting from 'accounting';

import View from './view';
import store from '../../store';
import actions from '../../actions';


class RecordsTable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			direction: -1
		}

		this.getRecordsList()

		this.deleteRecord.bind(this);
		this.editRecord.bind(this);
	}

	getRecordsList() {
		if(this.props.type == 'myRecords') {
			store.dispatch(actions.record.list());
			this.loadingFlag = this.props.myRecordsLoading;
		}
		if(this.props.type == 'allRecords') {
			store.dispatch(actions.record.all());
			this.loadingFlag = this.props.allRecordsLoading;
		}
	}

	render() {
		let recordsList =
			(this.props.type == 'myRecords') ?
			this.props.myRecordsList :
			this.props.allRecordsList;

		return (
			<View
				sortField="date"
				loading={ this.props.records[this.props.type + 'Loading'] }
				changeSort={ this.changeSort.bind(this) }
				sorter={ this.sorter.bind(this) }
				records={ this.buildTableData( recordsList ) }
				addLink={ this.props.addLink } />
		)
	}

	buildTableData(list) {
		if(!list || list.length == 0) return;

		let result = {
			labels: {
				email: (this.props.type == 'allRecords') ? 'Email' : false,
				date: 'Date',
				distance: 'Distance (metres)',
				time: 'Time',
				speed: 'Average speed (km/hr)',
			},
			items: list.map((item) => ({
				id: item._id,
				email: item.user ? item.user.email : '',
				role: item.user ? item.user.role : '',
				moment: moment(item.date),
				date: moment(item.date).format('DD-MM-YYYY'),
				distance: item.distance,
				time: item.time,
				seconds: item.seconds,
				speed: accounting.formatNumber((item.distance / 1000) / (item.seconds / 60 / 60), 2 , ' ')
			})),
			actions: [
				{ label: <i className="fa fa-pencil-square-o"></i>,
					header: 'Edit',
					action: this.editRecord.bind(this) },
				{ label: <i className="fa fa-trash-o"></i>,
					header: 'Delete', action: this.deleteRecord.bind(this) }
			]
		}

		return result;
	}

	sorter(a, b) {
		if(a.moment == b.moment) return 0;
		let result = (a.moment > b.moment) ? 1 : -1;
		return result * this.state.direction
	}

	changeSort() {
		this.setState({direction: this.state.direction * -1})
	}

	deleteRecord(record) {
		store.dispatch(actions.record.delete({"record": {"id": record.id}}))
	}

	editRecord(record) {
		this.props.history.push(`/records/edit/${record.id}/`)
	}
}

const mapStateToProps = (store) => {
	return {
		myRecordsList: store.records.myRecords,
		allRecordsList: store.records.allRecords,
		records: store.records
	};
}

export default connect(mapStateToProps)(withRouter(RecordsTable));
