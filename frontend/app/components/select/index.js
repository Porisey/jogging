import React from 'react';
import ReactSelect from 'react-select';

class Select extends React.Component {
	render() {
		const { options } = this.props;

		return (
			<ReactSelect { ...this.props } />
		)
	}
}

export default Select;
