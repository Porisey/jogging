import React from 'react';
import Picker from 'react-datetime';
import moment from 'moment';

import Tools from '../../tools';

class DatePicker extends React.Component {
	render() {
		const { name, dispatch, onChange, value, placeholder } = this.props;



		return (
			<Picker
				{ ...this.props }

				value={ moment(value) }
				dateFormat="DD/MM/YYYY"
				timeFormat={ false }
        onChange={ (val) => {
        	if(typeof val.format == 'function') {
	        	onChange(val.format())
        	}
        }}

        inputProps={{
        	className: this.props.className,
        	placeholder: placeholder,
        	ref: (node) => {
        		if(node == null) return;
						Tools.masks['date'].mask(node)
        	}
        }}

        className="" />
		)
	}
}

export default DatePicker;
