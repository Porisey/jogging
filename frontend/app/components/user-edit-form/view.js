import React from 'react';
import { Errors, Form, Control } from 'react-redux-form';
import Select from '../select';

export default function (props) {
	return (
		<div className="b-default-form">
			<div className="b-default-form__header">
				<div className="b-default-form__title">Edit an account</div>
			</div>
			<Form model="forms.user_edit" onSubmit={ (data) => { props.handleSubmit(data) } }
				validators={ props.validators } onChange={ (data) => { props.handleChanges(data) } } >

				<div className="b-default-form__section b-default-form__section_cols">

					<div className="b-default-form__half-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".firstname" className="u-input u-input_full" placeholder="First name"/>
						</div>
						<Errors className="b-default-form__errors" model=".firstname" { ...props.errorParams } />
					</div>


					<div className="b-default-form__half-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".surname" className="u-input u-input_full" placeholder="Surname" />
						</div>
						<Errors className="b-default-form__errors" model=".surname" { ...props.errorParams } />
					</div>


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".email" className="u-input u-input_full" placeholder="Email"/>
						</div>
						<Errors className="b-default-form__errors" model=".email" { ...props.errorParams } />
					</div>

					{ props.user.permissions['GET_USERS_LIST'] &&
						<div className="b-default-form__full-field">
							<div className="b-default-form__input-holder">
								<Control
									component={ Select }
									model=".role"
									options={ props.roles_list }
									placeholder="Role"/>
							</div>
							<Errors className="b-default-form__errors" model=".role" { ...props.errorParams } />
						</div>
					}


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.password type="password" model=".password" className="u-input u-input_full" placeholder="Change password"/>
						</div>
						<Errors className="b-default-form__errors" model=".password" { ...props.errorParams } />
					</div>

					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.password type="password" model=".repeat_password"
								className="u-input u-input_full" placeholder="Confirm password"/>
							<Errors className="b-default-form__errors" model="forms.user_edit"
								messages={{passconfirm: 'Check password are different'}}
								show={{ touched: true, focus: false}} />
						</div>
					</div>
				</div>

				{ props.formState.submitted &&
					<div className="b-default-form__section">Changes saved!</div>
				}

				<div className="b-default-form__section">
					<Errors className="b-default-form__errors" model="forms.user_edit" { ...props.errorParams } />
					<div className="b-default-form__submit-holder">
						<input className="u-button" type="submit" value="Save changes" />
					</div>
				</div>
			</Form>
		</div>
	)
}
