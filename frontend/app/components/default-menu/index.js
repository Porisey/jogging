import React from 'react';
import { connect } from 'react-redux';
import View from './view';

class DefaultMenu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (<View permissions={ this.props.user.permissions }/>)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current.userInfo
	};
};

export default connect(mapStateToProps)(DefaultMenu);
