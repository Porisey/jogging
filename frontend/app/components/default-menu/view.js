import React from 'react';
import Components from '../';
import { Route, Switch, Link } from 'react-router-dom';

export default function (props) {
	return (
		<div className="b-default-menu">
			<div className="b-default-menu__first-holder">
				<ul className="b-default-menu__first-level">
					<li className="b-default-menu__first-item">
						<Link to="/records/" className="b-default-menu__first-link">Records</Link>
					</li>
					<li className="b-default-menu__first-item">
						<Link to="/reports/" className="b-default-menu__first-link">Reports</Link>
					</li>

					{ props.permissions['GET_USERS_LIST'] &&
						<li className="b-default-menu__first-item">
							<Link to="/users/" className="b-default-menu__first-link">Users</Link>
						</li>
					}

					{ props.permissions['GET_RECORDS_LIST'] &&
						<li className="b-default-menu__first-item">
							<Link to="/records/all/" className="b-default-menu__first-link">All Records</Link>
						</li>
					}
				</ul>
			</div>
		</div>
	);
}
