import React from 'react';
import { connect } from 'react-redux';
import View from './view';

class Users extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (
			<View match={this.props.match} user={ this.props.user }/>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current
	};
}

export default connect(mapStateToProps)(Users);
