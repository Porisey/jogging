import React from 'react';
import Components from '../';
import { Redirect, Route, Switch, Link } from 'react-router-dom';

export default function (props) {
	return (
		<main className="b-layout__main">
			<div className="u-container">
				<Switch>
					<Route path='/users/add/' render={() => {
						return (
							<div className="b-layout__item b-layout__item_small">
								{ props.user.isAuthorized && <Redirect to="/records/"/>}
								<Components.RegistrationForm />
							</div>
						)
					}}/>

					{ !props.user.isAuthorized && <Redirect to="/users/add"/>}

					<Route path='/users/edit/:id' render={({ match }) => {
						return (
							<div className="b-layout__item b-layout__item_small">
								<Components.UserEditForm targetId={ match.params.id } formType="edit"/>
							</div>
						)
					}}/>

					<Route path='/users/create' render={({ match }) => {
						return (
							<div className="b-layout__item b-layout__item_small">
								<Components.RegistrationForm />
							</div>
						)
					}}/>
				</Switch>


				<Route exact path='/users/' render={() => {
					return (
						<div>
							<div className="b-layout__item">
								<Link to='/users/create/'>
									<i className="fa fa-plus-square-o"></i>
									<span> Add user</span>
								</Link>
							</div>
							<div className="b-layout__item">
								<Components.UsersTable />
							</div>
						</div>
					)
				}}/>
			</div>
		</main>
	);
}
