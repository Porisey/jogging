import React from 'react';
import { connect } from 'react-redux';
import View from './view';

class Records extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (
			<View user={ this.props.user } match={ this.props.match }/>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: store.users.current
	};
}

export default connect(mapStateToProps)(Records);
