import React from 'react';
import Components from '../';
import { Route, Switch, Link } from 'react-router-dom';

export default function (props) {
	return (
		<main className="b-layout__main">
			{ props.user.isAuthorized &&
				<div className="u-container">
					<Route exact path='/records/' render={() => (
							<div className="b-layout__item">
								<Components.RecordsTable type="myRecords" addLink="/records/add/"/>
							</div>
					)}/>

					<Route exact path='/records/all' render={() => (
							<div className="b-layout__item">
								<Components.RecordsTable type="allRecords" addLink="/records/add/"/>
							</div>
					)}/>

					<Route path="/records/edit/:id" render={({match}) => (
							<div className="b-layout__item b-layout__item_small">
								<Components.RecordEditForm targetId={ match.params.id }/>
							</div>
					)}/>

					<Route path="/records/add/" render={() => (
							<div className="b-layout__item b-layout__item_small">
								<Components.RecordForm formType="add"/>
							</div>
					)}/>
				</div>
			}
		</main>
	);
}
