import React from 'react';
import { connect } from 'react-redux';
import accounting from 'accounting';
import { Map } from 'immutable';
import moment from 'moment';

import View from './view';
import store from '../../store';
import actions from '../../actions';


class ReportsTable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			direction: -1
		}

		this.getRecordsList()
	}

	getRecordsList() {
		store.dispatch(actions.record.list())
	}

	render() {
		let report = this.buildReport(this.props.recordsList);

		return (
			<View
				sortField="week"
				loading={ this.props.loading }
				changeSort={ this.changeSort.bind(this) }
				sorter={ this.sorter.bind(this) }
				tableData={ this.buildTableData( report ) } />
		)
	}

	buildTableData(list) {
		if(!list || list.length == 0) return;

		let result = {
			labels: {
				week: 'Week',
				distance: 'Average distance',
				speed: 'Average speed'
			},
			items: list.map((item) => ({
				week: item.week,
				distance: accounting.formatNumber((item.distance / item.qtty), 2, ' '),
				speed: accounting.formatNumber((item.distance / 1000) / (item.seconds / 60 / 60), 2, ' ')
			})),
			actions: []
		}

		return result;
	}

	buildReport(records) {
		let result = Map();

		records.forEach((record) => {
			let date = moment(record.date);
			let week = +(date.format('w'));
			let entry = result.get(week);


			if(entry == undefined) {
				result = result.set(week, {
					week: week,
					seconds: record.seconds,
					distance: +record.distance,
					qtty: 1
				})
			} else {
				result = result.set(week, {
					week: week,
					seconds: entry.seconds + record.seconds,
					distance: +record.distance + entry.distance,
					qtty: ++entry.qtty
				})
			}
		})

		return result;
	}

	sorter(a, b) {
		return (a.week - b.week) * this.state.direction;
	}

	changeSort() {
		this.setState({direction: this.state.direction * -1})
	}
}

const mapStateToProps = (store) => {
	return {
		recordsList: store.records.myRecords,
		loading: store.records.myRecordsLoading
	};
}

export default connect(mapStateToProps)(ReportsTable);
