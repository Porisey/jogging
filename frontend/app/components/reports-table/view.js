import React from 'react';
import Components from '../';

export default function (props) {
	return (
		<div>
			<Components.ActionTable { ...props }/>
		</div>
	);
}
