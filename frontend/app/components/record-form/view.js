import React from 'react';
import { Errors, Form, Control } from 'react-redux-form';
import DatePicker from '../datepicker';
import Inputmask from '../inputmask';

export default function (props) {
	return (
		<div className="b-default-form">
			<div className="b-default-form__header">
				<div className="b-default-form__title">Add record</div>
			</div>

			<Form model="forms.record" onSubmit={ (data) => { props.handleSubmit(data); } }
				validators={ props.validators } onChange={ (data) => { props.handleChanges(data) } } >

				<div className="b-default-form__section b-default-form__section_cols">
					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text model=".date" component={ DatePicker }
								className="u-input u-input_full" placeholder="Date"/>
						</div>
						<Errors className="b-default-form__errors" model=".date" { ...props.errorParams } />
					</div>


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control.text  model=".distance" className="u-input u-input_full" placeholder="Distance"
								getRef={ (node) => props.initMask('number', node) } />
						</div>
						<Errors className="b-default-form__errors" model=".distance" { ...props.errorParams } />
					</div>


					<div className="b-default-form__full-field">
						<div className="b-default-form__input-holder">
							<Control model=".time" className="u-input u-input_full" placeholder="Time"
								component={ Inputmask } mask="99:99:99"/>
						</div>
						<Errors className="b-default-form__errors" model=".time" { ...props.errorParams } />
					</div>


				</div>
				{ props.formState.submitted && <div className="b-default-form__section">Record saved!</div> }
				<div className="b-default-form__section">
					<Errors className="b-default-form__errors" model="forms.registration" { ...props.errorParams } />
					<div className="b-default-form__submit-holder">
						<input className="u-button" type="submit" value="SUBMIT" />
					</div>
				</div>
			</Form>
		</div>
	)
}
