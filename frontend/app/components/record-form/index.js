import React from 'react';
import { connect } from 'react-redux';
import { actions as formActions } from 'react-redux-form';

import store from '../../store';
import actions from '../../actions';
import Tools from '../../tools';
import View from './view';

class RecordForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.defineErrorParams();
		this.defineFormValidators();
	}

	render() {
		return (
			<View
				formType={ this.props.formType }
				formState={ this.props.formState }
				formData={ this.props.formData }
				handleSubmit={ this.handleSubmit.bind(this) }
				handleChanges={ this.handleChanges.bind(this) }
				initMask={ this.initMask.bind(this) }
				validators={ this.validators }
				errorParams={ this.errorParams }/>
		)
	}

	handleSubmit(formData) {
		store.dispatch(actions.record.create({"record": formData}));
	}

	handleChanges() {
		this.resetSubmit(this.props.formState);
		this.resetServerErrors(this.props.formState);
	}

	resetSubmit(formState) {
		if(formState.submitted)
			store.dispatch(formActions.setSubmitted('forms.record', false));
	}

	componentWillUnmount() {
		this.handleChanges()
	}

	resetServerErrors(formState) {
		if(formState.errors.serverError)
			store.dispatch(formActions.setErrors('forms.record', { serverError: false }));
	}

	defineFormValidators() {
		let required = Tools.formValidators.required;
		let isEmail = Tools.formValidators.isEmail;

		this.validators = {
			date: { required },
			distance: { required },
			time: { required },
		}
	}

	defineErrorParams() {
		this.errorParams = {
			messages: {
				required: 'Required field',
				isEmail: 'Incorrect email',
				serverError: 'Server unavailable'
			},
			show: {
				touched: true,
				focus: false
			}
		}
	}

	initMask(type, node) {
		if(node == null) return;
		Tools.masks[type].mask(node)
	}

	parseTime(timeString) {
	}
}

const mapStateToProps = (store) => {
	return {
		formState: store.forms.forms.record.$form,
		formData: store.forms.record
	};
};

export default connect(mapStateToProps)(RecordForm);
