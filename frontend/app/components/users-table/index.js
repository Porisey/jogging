import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import View from './view';
import store from '../../store';
import actions from '../../actions';


class UsersTable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
		this.getUserList()

		this.deleteUser.bind(this);
		this.editUser.bind(this);
	}

	getUserList() {
		if(this.props.user.isAuthorized)
			store.dispatch(actions.user.list())
	}

	render() {
		return (
			<View loading={ this.props.usersLoading } users={ this.buildTableData( this.props.usersList ) }/>
		)
	}

	buildTableData(list) {
		if(!list || list.length == 0) return;

		let result = {
			labels: {
				email: 'Email',
				firstname: 'First name',
				surname: 'Last name',
				role: 'Role'
			},
			items: list.map((item) => ({
				id: item._id,
				email: item.email,
				firstname: item.firstname,
				surname: item.surname,
				role: item.role
			})),

			actions: [
				{ label: <i className="fa fa-pencil-square-o"></i>,
					header: 'Edit', action: this.editUser.bind(this) },
				{ label: <i className="fa fa-trash-o"></i>,
					header: 'Delete', action: this.deleteUser.bind(this) }
			]
		}

		return result;
	}

	deleteUser(user) {
		store.dispatch(actions.user.delete(user.id))
	}

	editUser(user) {
		this.props.history.push(`/users/edit/${user.id}/`)
	}
}

const mapStateToProps = (store) => {
	return {
		usersList: store.users.list.items,
		user: store.users.current,
		usersLoading: store.users.list.loading
	};
}

export default connect(mapStateToProps)(withRouter(UsersTable));
