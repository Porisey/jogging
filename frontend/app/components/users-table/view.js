import React from 'react';
import Components from '../';

export default function (props) {
	return (
		<Components.ActionTable { ...props } tableData={ props.users } />
	);
}
