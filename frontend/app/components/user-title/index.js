import React from 'react';
import { connect } from 'react-redux';

import View from './view';
import Tools from '../../tools';
import store from '../../store';
import actions from '../../actions';

class UserTitle extends React.Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	render() {
		return (
			<View
				user={ this.props.userData }
				formData={ this.props.formData}
				handleSubmit={ this.handleSubmit.bind(this) }
				logoutHandler={ this.logoutHandler.bind(this) }
				validators={ this.defineFormValidators() }
				errorParams={ this.defineErrorParams() } />
		)
	}

	handleSubmit(formData) {
		store.dispatch(actions.user.login({ "auth": formData }))
	}

	logoutHandler() {
		store.dispatch(actions.user.logout())
	}

	defineFormValidators() {
		let required = Tools.formValidators.required;
		let isEmail = Tools.formValidators.isEmail;

		return {
			email: { required, isEmail },
			password: { required }
		}
	}

	defineErrorParams() {
		return {
			messages: {
				required: 'Required field',
				isEmail: 'Incorrect email',
			},
			show: {
				touched: true,
				focus: false
			}
		}
	}
}

const mapStateToProps = (store) => {
	return {
		formData: store.forms.forms.auth,
		userData: store.users.current
	};
}

export default connect(mapStateToProps)(UserTitle);
