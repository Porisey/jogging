import React from 'react';
import { Errors, Form, Control } from 'react-redux-form';

export default function (props) {
	return (
		<div className="b-user-title">
			{ !props.user.isAuthorized &&
				<div className="b-user-title__login-section">
					<Form validators={ props.validators } model="forms.auth" onSubmit={(data) => props.handleSubmit(data)} >
						<div className="b-user-title__form-fields">
							<div className="b-user-title__form-field">
								<Control.text model=".email"
									className={"b-user-title__text-input u-input u-input_full" +
										((!props.formData.email.valid && props.formData.email.submitFailed) ? ' u-input_error' : '')}
									placeholder="Email"/>
							</div>
							<div className="b-user-title__form-field">
								<Control.password model=".password"
									className={"b-user-title__text-input u-input u-input_full" +
										((!props.formData.password.valid && props.formData.password.submitFailed) ? ' u-input_error' : '')}
									placeholder="Password"/>
							</div>
						</div>
						<div className="b-user-title__submit">
							<button className="b-user-title__submit-button u-button" type="submit">Log In</button>
						</div>
					</Form>
				</div>
			}
			{ props.user.isAuthorized &&
				<div className="b-user-title__logout-section">
					{ props.user.userInfo.firstname &&
						<span>Hello, {props.user.userInfo.firstname }</span>
					}
					<button className="b-user-title__logout-btn u-button" onClick={props.logoutHandler}>Logout</button>
				</div>
			}

			<Errors className="b-user-title__errors-section" model="forms.auth"
				messages={{
					authError: 'Incorrect email/password'
				}} />
		</div>
	)
}
