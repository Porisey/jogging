import React from 'react';
import InputMask from 'react-input-mask';

class Inputmask extends React.Component {
	render() {
		let maskChar = this.props.maskChar || '_';

		switch(this.props.mask) {
			default:
				return (<InputMask { ...this.props} />);
		}
	}

	static convertTimeInSeconds(val) {
		let time = 0;

		if(val && val.length == 8) {
			let splitted = val.split(':')

			return splited.reduce((all, val, i) => {
				let multiplier = Math.pow(60, splited.length - (++i));
			 	return all += +val * multiplier;
			})
		} else {
			return val
		}
	}

	convertSecondsInTime(val) {
		let seconds = val % Math.pow(60, 3);
		let minutes = (val - seconds*60) % Math.pow(60, 2);

	}
}

export default Inputmask;
