import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';

import {
	BrowserRouter,
	Route, Switch,
	browserHistory as history,
	HashRouter } from 'react-router-dom';

import App from './components/app';

let Router = BrowserRouter;

ReactDOM.render((
	<Provider store={store}>
		<Router history={history}>
			<Switch>
				<Route path="/" component={App} />
			</Switch>
		</Router>
	</Provider>
), document.getElementById('root'));
