import Inputmask from 'inputmask'
import moment from 'moment'

const singleton = Symbol();
const singletonCheck = Symbol();

export class Tools {
	static get instance() {
		if(this[singleton] === undefined) this[singleton] = new Tools(singletonCheck);
		return this[singleton];
	}

	constructor(check) {
		if(check != singletonCheck) {
			throw new Error("Don't try to create singleton manually");
		}

		this.defineInputMasks()
	}

	get formValidators() {
		return {
			required: (val) => {
				return !!val },
			isEmail: (val) => {
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(val);
			}
		}
	}

	defineInputMasks() {
		this.masks =  {
			number: new Inputmask({ regex: '[0-9]*'}),

			time: new Inputmask('99:99:99', {
				placeholder: '_',
				showMaskOnHover: false,
				onincomplete: function() { this.value = '' }
			}),

			date: new Inputmask('dd/mm/yyyy',
				{ showMaskOnHover: false,
					placeholder: '__/__/____',
					onincomplete: function(){
            this.value = '';
          }
				})
		}
	}

	parseTime(timeString) {
		let duration = moment.duration(timeString, 'hh:mm:ss')
		return duration.as('seconds')
	}

	parseSeconds(time) {
		let duration = moment.duration(time, 'seconds');

		let hours = ('0' + duration.hours()).slice(-2);
		let minutes = ('0' + duration.minutes()).slice(-2);
		let seconds = ('0' + duration.seconds()).slice(-2);

		return `${hours}:${minutes}:${seconds}`;
	}
}

export default Tools.instance;
