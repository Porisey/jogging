import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import userReducer from './reducers/users';
import formReducer from './reducers/forms';
import recordReducer from './reducers/records';

let reducer = combineReducers({
	users: userReducer,
	forms: formReducer,
	records: recordReducer
})

export default createStore(reducer, applyMiddleware(thunk));
