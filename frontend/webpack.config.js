var webpack = require('webpack');
var path = require('path');

module.exports = {
  cache: true,
  watch: true,
  entry: {
    application: './app/index.js'
  },
  output: {
    path: path.resolve(__dirname, "../build/assets/js/"),
    // Filename for entry points
    // Only adds hash in build mode
    filename: '[name].bundle.js',
  },
  module: {
    loaders: [
      {
        // JSON LOADER
        // Reference: https://github.com/webpack/json-loader
        test: /\.json$/,
        loader: 'json-loader',
        exclude: /node_modules/
      },
      {
        // JS LOADER
        // Reference: https://github.com/babel/babel-loader
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        // ASSET LOADER
        // Reference: https://github.com/webpack/file-loader
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file-loader'
      }, {
        // HTML LOADER
        // Reference: https://github.com/webpack/raw-loader
        test: /\.html$/,
        loader: 'raw-loader',
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      ScrollMagic:'scrollmagic',
      moment: 'moment'
    })
  ]
}
