var gulp = require('gulp');
var sync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');
var prettify = require('gulp-prettify');
var ignore = require('gulp-ignore');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var webpack = require('webpack-stream');
var path = require('path');
var sequence = require('run-sequence');
var watch = require('gulp-watch');

var destPath = '../public/';

gulp.task('default', function () {
  return sequence(
    ['pug', 'stylus', 'js'],
    'server',
    'watch'
  )
});

gulp.task('server', function () {
	return sync.init({
		open: true,
		notify: false,
		directory: true,
		server: {
			baseDir: [destPath]
		},
		delay: 0
	})
})

gulp.task('pug', function () {
	return gulp.src(['_markup/templates/*.pug'])
		.pipe(plumber())
		.pipe(pug({ basedir: './', verbose: true, pretty: false}))
    .pipe(prettify({indent_char: '\t', indent_size: 1, unformatted: [] }))
		.pipe(gulp.dest(path.resolve(destPath)))
		.on('end', sync.reload);
})

gulp.task('stylus', function () {
	return gulp.src('_markup/style/application.styl')
		.pipe(plumber())
		.pipe(ignore('**/_*.styl'))
		.pipe(stylus({
			'include css': true
		}))
		.on('error', function (error) { console.error(error); })
		.pipe(autoprefixer({
			browsers: ['last 2 versions', 'safari 5', 'ie 10', 'opera 12.1', 'ios 6', 'android 4']
		}))
		.pipe(rename({
			suffix: '.bundle'
		}))
		.pipe(gulp.dest(path.resolve(destPath + 'assets/css/')))
		.pipe(sync.stream())
})

gulp.task('js', function () {
	gulp.src(['add/index.js'])
		.pipe(plumber())
		.pipe(webpack(require('./webpack.config.js')))
		.pipe(gulp.dest(path.resolve(destPath + 'assets/js/')))
		.pipe(sync.stream())
})


gulp.task('watch', function () {
  watch(['_markup/**/*.pug'], function () {
    sequence('pug')
  })
  watch(['_markup/**/*.styl'], function () {
    sequence('stylus')
  })
});
